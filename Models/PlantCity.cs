namespace PlantService.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    public class PlantCity
    {
        [Required]
        [MaxLength(100)]
        public string City { get; set; }
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime Start_Date{get;set;}
        [Required]
        public DateTime End_Date { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        [MaxLength(50)]
        public string Status { get; set; }
        [MaxLength(50)]
        public string Color { get; set; }
    }
}