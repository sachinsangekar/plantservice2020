namespace PlantService.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using PlantService.Models;
    using PlantService.Dtos;
    using PlantService.DataContext;
    using PlantService.Repository;
    using Microsoft.EntityFrameworkCore;
    using AutoMapper;

    public class CityService : IPlantCityRepo
    {
        private readonly PlantServiceContext _context;
        private readonly IMapper _mapper;

        public CityService(PlantServiceContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ICollection<PlantCityDto> GetAll()
        {
            ICollection<PlantCityDto> plantCityDto = new List<PlantCityDto>();
            
            foreach (var city in _context.PlantCities)
            {
                plantCityDto.Add(_mapper.Map<PlantCityDto>(city));
            }
            return plantCityDto;            
        }

        public PlantCityDto GetById(int Id)
        {
            PlantCityDto plantCityDto = default(PlantCityDto);
            var city = _context.PlantCities.FirstOrDefault(s => s.Id == Id);
            if(city != null)
            {
                plantCityDto = _mapper.Map<PlantCityDto>(city);
            }
            return plantCityDto;
        }

        public ICollection<PlantCityDto> SeedDatabase(PlantCity[] cities)
        {
            _context.PlantCities.AddRange(cities);            
            int result = _context.SaveChanges();
            ICollection<PlantCityDto> plantCityDto = new List<PlantCityDto>();
            if(result > 0)
            {
                foreach (var city in _context.PlantCities)
                {
                    plantCityDto.Add(_mapper.Map<PlantCityDto>(city));
                }
            }
            return plantCityDto;
        }

        public ICollection<PlantCityDto> SearchCity(DateTime startDate, DateTime endDate)
        {
            ICollection<PlantCityDto> plantCityDto = new List<PlantCityDto>();
            var cities = _context.PlantCities
                            .Where(pc => (pc.Start_Date >= startDate && pc.Start_Date <= endDate) && 
                                (pc.End_Date >= startDate && pc.End_Date <= endDate));

                            // .Where(obj => obj.Start_Date >= startDate && obj.End_Date <= endDate);

            foreach (var city in cities)
            {
                plantCityDto.Add(_mapper.Map<PlantCityDto>(city));
            }
            return plantCityDto;                       
        }

        public bool Delete(int id)
        {
            int result = 0;
            var city = _context.PlantCities.FirstOrDefault(s => s.Id == id);      
            if(city != null)  
            {
                _context.PlantCities.Remove(city);
                result = _context.SaveChanges();
            }

            if(result > 0)
                return true;
            else
                return false;
        }
    }
}