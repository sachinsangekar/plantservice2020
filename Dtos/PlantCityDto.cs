namespace PlantService.Dtos
{
    using System;
    using System.ComponentModel.DataAnnotations;
    public class PlantCityDto
    {
        public string City { get; set; }
        public int Id { get; set; }        
        public DateTime Start_Date{get;set;}        
        public DateTime End_Date { get; set; }
        public double Price { get; set; }
        public string Status { get; set; }        
        public string Color { get; set; }
    }
}