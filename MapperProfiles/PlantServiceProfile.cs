namespace PlantService.MapperProfiles
{
    using AutoMapper;
    using PlantService.Models;
    using PlantService.Dtos;

    public class PlantServiceProfile : Profile
    {
        public PlantServiceProfile()
        {
            CreateMap<PlantCity, PlantCityDto>();
        }
    }
}