namespace PlantService.DataContext
{
    using Microsoft.EntityFrameworkCore;
    using PlantService.Models;

    public class PlantServiceContext : DbContext
    {
        public PlantServiceContext(DbContextOptions<PlantServiceContext> opt) : base(opt)
        {

        }

        public DbSet<PlantCity> PlantCities { get; set; }        
    }
}