namespace PlantService.Repository
{
    using System;
    using PlantService.Models;
    using PlantService.Dtos;
    using System.Collections.Generic;
    
    public interface IPlantCityRepo
    {
        ICollection<PlantCityDto> GetAll();
        PlantCityDto GetById(int id);
        bool Delete(int id);
        ICollection<PlantCityDto> SearchCity(DateTime startDate, DateTime endDate);
        ICollection<PlantCityDto> SeedDatabase(PlantCity[] cities);
    }
}