namespace PlantService.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using PlantService.DataContext;
    using PlantService.Models;
    using PlantService.Dtos;
    using PlantService.Repository;

    [ApiController]
    [Route("[controller]")]
    public class CityController : ControllerBase
    {
        private const string SearchRouteParam = "{startDate}/{endDate}";
        private const string IdRouteParam = "{id:int}";

        private readonly ILogger<CityController> _logger;
        private readonly IPlantCityRepo _repository;

        public CityController(ILogger<CityController> logger, IPlantCityRepo repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var cities = _repository.GetAll();
                return Ok(cities);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred while retrieving cities, Exception:{ex.Message}");                
                throw;
            }
        }

        [HttpGet(IdRouteParam)]
        public IActionResult GetById(int Id)
        {
            try
            {
                var city = _repository.GetById(Id);
                return Ok(city);                
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred while retrieving city, Exception:{ex.Message}");                
                throw;
            }
        }

        [HttpGet(SearchRouteParam)]
        public IActionResult SearchCity(DateTime startDate, DateTime endDate)
        {
            try
            {
                var cities = _repository.SearchCity(startDate, endDate);
                return Ok(cities);                
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred while retrieving secrch city between dates, Exception:{ex.Message}");                
                throw;
            }
        }

        [HttpPost]
        public IActionResult SeedDatabase(PlantCity[] citiesDto)
        {
            try
            {
                var cities = _repository.SeedDatabase(citiesDto);            
                return Ok(cities);   
            }
            catch (Exception ex)
            {                
                _logger.LogError($"Error occurred while retrieving city data, Exception:{ex.Message}");
                throw;
            }
        }

        [HttpDelete(IdRouteParam)]
        public IActionResult DeleteCity(int id)
        {
            try
            {
                var result = _repository.Delete(id);            
                return Ok(result);   
            }
            catch (Exception ex)
            {                
                _logger.LogError($"Error occurred while deleting city, Exception:{ex.Message}");
                throw;
            }
        }
    }
}