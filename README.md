#API Setup

1. Install Dotnet Core 3.1 (Use terminal to install nuget or download from site https://dotnet.microsoft.com/download/dotnet-core/3.1)
2. Download repo PlantService.
3. Open folder in VS Code.
4. Setup project and install nuget packages available in .csproj file.
5. To create database run "dotnet ef database update".
6. To run API use dotnet run

*Note: Must have SQL Server.